import logging
import sys

from fastapi import FastAPI, __version__ as fastapi_version

from routers.celery import router as celery_router
from routers.loggers import router as loggers_router
from routers.users import router as users_router
from routers.users_settings import router as users_settings_router
from settings import database

version = f"{sys.version_info.major}.{sys.version_info.minor}"

logger = logging.getLogger(__name__)


app = FastAPI()

app.include_router(celery_router, prefix="/celery", tags=["Celery"])
app.include_router(users_router, prefix="/users", tags=["Users"])
app.include_router(loggers_router, prefix="/loggers", tags=["Loggers"])
app.include_router(users_settings_router, prefix="/settings", tags=["Settings"])


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
async def read_root():
    logger.debug("debug message")
    logger.info("info message")
    logger.warning("warn message")
    logger.error("error message")
    logger.critical("critical message")
    message = f"Hello euri10! From FastAPI {fastapi_version} running on Uvicorn with Gunicorn in Alpine. Using Python {version}"
    return {"message": message}


@app.get("/healthcheck")
async def healthcheck():
    return {"status": "alive"}

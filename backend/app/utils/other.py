from typing import List

from fastapi.routing import APIRoute
from starlette.routing import NoMatchFound


def status_for(routeList: List[APIRoute], name: str, **path_params: str) -> int:
    for route in routeList:
        try:
            route.url_path_for(name, **path_params)
            return route.status_code
        except NoMatchFound:
            pass
    raise NoMatchFound()

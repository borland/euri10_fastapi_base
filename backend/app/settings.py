from cryptography.fernet import Fernet
from databases import Database, DatabaseURL
from starlette.config import Config
from starlette.datastructures import Secret

config = Config()

DEBUG = config("DEBUG", cast=bool, default=False)
TESTING = config("TESTING", cast=bool, default=False)

POSTGRES_USER = config("POSTGRES_USER", cast=str, default="postgres")
POSTGRES_PASSWORD = config("POSTGRES_PASSWORD", cast=Secret, default="postgres")
POSTGRES_SERVER = config("POSTGRES_SERVER", cast=str, default="db")
POSTGRES_PORT = config("POSTGRES_PORT", cast=str, default="5432")
POSTGRES_DB = config("POSTGRES_DB", cast=str, default="postgres")

DATABASE_URL = config(
    "DATABASE_URL",
    cast=DatabaseURL,
    default=f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}",
)
DATABASE_TEST_URL = config(
    "DATABASE_TEST_URL",
    cast=DatabaseURL,
    default=f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}_test",
)


# Use 'force_rollback' during testing, to ensure we do not persist database changes
# between each test case.
if TESTING:
    database = Database(DATABASE_TEST_URL, force_rollback=True)
else:
    database = Database(DATABASE_URL)

PAGINATION_KEY = config("PAGINATION_KEY", cast=Secret)

fernet = Fernet(str(PAGINATION_KEY).encode())

RABBITMQ_DEFAULT_USER = config("RABBITMQ_DEFAULT_USER", cast=str)
RABBITMQ_DEFAULT_PASS = config("RABBITMQ_DEFAULT_PASS", cast=Secret)

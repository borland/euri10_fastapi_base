from main import app
from models.users_settings import AvailableLang, AvailableTheme, UserSettingsIn


def test_create_users_settings(client, register_one_user):
    url = app.url_path_for(
        "users_settings_create", email_or_name=register_one_user.name
    )
    user_one_settings = UserSettingsIn(
        lang=AvailableLang.en,
        theme=AvailableTheme.dark,
        avatar="https://avatarurl.com/avatar.png",
    )
    response = client.post(url, json=user_one_settings.dict())
    assert response.status_code == 201
    assert response.json() == user_one_settings.dict()


def test_create_users_settings_extra_settings_key(client, register_one_user):
    url = app.url_path_for(
        "users_settings_create", email_or_name=register_one_user.name
    )
    user_one_settings = UserSettingsIn(
        lang=AvailableLang.en,
        theme=AvailableTheme.dark,
        avatar="https://avatarurl.com/avatar.png",
    )
    user_one_settings_extra_key = user_one_settings.dict()
    user_one_settings_extra_key.update({"key": "value"})
    response = client.post(url, json=user_one_settings_extra_key)
    assert response.status_code == 422


def test_list_users_settings(client, register_one_user_with_settings):
    url = app.url_path_for(
        "users_settings_list", email_or_name=register_one_user_with_settings.name
    )
    response = client.get(url)
    assert response.status_code == 200
    expected_settings = UserSettingsIn(
        lang=AvailableLang.en,
        theme=AvailableTheme.dark,
        avatar="https://avatarurl.com/avatar.png",
    )
    assert response.json() == expected_settings.dict()

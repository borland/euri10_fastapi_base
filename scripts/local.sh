#! /usr/bin/env bash

set -euxo pipefail

sudo find . -type d -name __pycache__ -exec rm -r {} \+
docker-compose -f docker-compose-dev.yml up -d --build
docker-compose -f docker-compose-dev.yml down -v --remove-orphans
docker-compose -f docker-compose-dev.yml up -d
docker-compose -f docker-compose-dev.yml exec -T backend-tests pytest tests -v
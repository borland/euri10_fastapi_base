#! /usr/bin/env bash

set -euxo pipefail

sudo find . -type d -name __pycache__ -exec rm -r {} \+
eval $(AWS_PROFILE=perso docker-machine env euri10fastapibase)
docker-compose -f docker-compose-prod.yml pull
docker-compose -f docker-compose-prod.yml up -d --build